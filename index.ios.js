/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import Header from './src/components/Header';
import ImageDisplay from'./src/components/ImageDisplay';
import MainScreen from'./src/components/MainScreen';

import {StackNavigator}  from 'react-navigation';
import SecondScreen from './src/components/SecondScreen';
import EncodeScreen from './src/components/EncodeScreen';




import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity,
} from 'react-native';

const NavigationApp=StackNavigator({
  Home:{screen:ImageDisplay},
  Encode:{screen:EncodeScreen},

});

const App =(props) =>{

    return (
      <NavigationApp/>
    );
}


AppRegistry.registerComponent('second_app', () => App);
