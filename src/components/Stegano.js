import React from 'react';
import {Text,View,TouchableOpacity} from 'react-native'

const Stegano=(props)=>{
  return(
    <TouchableOpacity style={styles.myView} onPress={props.onPress}>
      <Text style={styles.myText}>
        {props.getBodyText}
      </Text>
    </TouchableOpacity>
  );
}
const styles={
  myText:{
    fontSize:25,
    color:'#00f',
    justifyContent:'center',
    alignItems:'center'
  },
  myView:{
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    alignItems:'center',
    borderRadius:5,
    backgroundColor:'#fff',
    borderColor:'#007aff',
    shadowOpacity:0.8,
    height:50,
    width:325,
    marginTop:15,
    marginLeft:10,
    padding:5
  }
}
export default Stegano;
