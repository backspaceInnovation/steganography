
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PixelRatio,
  TouchableOpacity,
  Image,
  Alert,
  AsyncStorage,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { StackNavigator } from "react-navigation";
import Header from './Header';



class ImageDisplay extends Component{
  constructor(props){
    super(props);
  }

  // navigateToOtherScreen(){
  //   var props='SecondScreen';
  //   Alert.alert(props);

  //   // const navigate={
  //   //   navigate:props.navigate
  //   // };
  //   const { navigate } = props.navigation;
  // }
  
  //const { navigate } = {props.navigation};

 
  static navigationOptions={
    title:'Home',
  };
  state = {
    ImageSource: null,
    imageBase:null,
    whichScreen:null,
  };

    selectPhotoTapped() {
      this.setState({
        whichScreen:1,
      });
      const options = {
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };
      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
  
        if (response.didCancel) {
          console.log('User cancelled photo picker');
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
          let source = { uri: response.uri };
  
          // You can also display the image using data:
          // let source = { uri: 'data:image/jpeg;base64,' + response.data };
          let base = { uri: 'data:image/jpeg;base64,' + response.data };
          let anotherBase=response.data;
           let imagePath='file'+response.path;
          this.setBaseImage(anotherBase,imagePath);
          this.setState({
            ImageSource: source,
            imageBase:anotherBase,
          });
        }
      });
    }


showAlert(){
  Alert.alert('You pressed the next Hammer');

}


//Take images from gallery only
  gallerySelected() {
    this.setState({
      whichScreen:2,
    });
      const options = {
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
        // storageOptions: {
        //   skipBackup: true
        // }
      };
      ImagePicker.launchImageLibrary(options, (response) => {
        console.log('Response = ', response);
  
        if (response.didCancel) {
          console.log('User cancelled photo picker');
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
          let src = { uri: response.uri };
          // You can also display the image using data:
          // let source = { uri: 'data:image/jpeg;base64,' + response.data };
          let base = { uri: 'data:image/jpeg;base64,' + response.data} ;
          let anotherBase=response.data;
          let imagePath='file'+response.path;
          this.setBaseImage(anotherBase,imagePath);
          this.setState({
            ImageSource: src,
            imageBase:anotherBase,
          });
        }
      });
  }
  ImageText(){
    Alert.alert(this.state.ImageSource.string());
  }

  navigateToOtherScreen(){
     //navigate('Encode',{img:this.state.ImageSource});
    const {navigate}=this.props.navigation;
    //Alert.alert(this.state.whichScreen);

    if(this.state.ImageSource==null){
      Alert.alert('Please select a photo first and then proceed');
    }else{
      if(this.state.whichScreen==1){
        navigate('Encode',{img:this.state.ImageSource});
      }else if(this.state.whichScreen==2){
        navigate('Decode',{img:this.state.ImageSource});
      }else{
        Alert.alert('Something went wrong');
      }
    }
  }

    async setBaseImage(base,source){
      await AsyncStorage.setItem('@MyBase64Image',base);
      await AsyncStorage.setItem('@MyImageName',source);
     
    }

  render() {
    const {navigate}=this.props.navigation;
      return (
        <View style={styles.container}>
        
          <View>
            <TouchableOpacity style={styles.myView} 
            onPress={this.selectPhotoTapped.bind(this)}>
            <Text style={styles.myText}>Steganography</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.myView} 
            onPress={this.gallerySelected.bind(this)}>
            <Text style={styles.myText}>Steganoanalysis</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.ImageContainer}>
            { this.state.ImageSource === null ? <Text>Select a Photo</Text> :
              <Image style={styles.ImageContainer} source={this.state.ImageSource} />}
          </View>

          <View style={styles.myButton}>
            <TouchableOpacity style={styles.buttonStyle} 
            /*onPress={()=>navigate('Encode',{img:this.state.ImageSource})}>*/
            onPress={this.navigateToOtherScreen.bind(this)}>
             <Text style={styles.myText}>
                Next
              </Text>
            </TouchableOpacity>
           </View>
        </View>
      );
    }
}

const styles ={

    container: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },

    ImageContainer: {
      borderRadius: 10,
      marginTop:20,
      width: 250,
      height: 280,
      borderColor: '#9B9B9B',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      
    },
    myView:{
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderRadius:5,
    backgroundColor: '#F5FCFF',
    borderColor:'#007aff',
    shadowOpacity:0.8,
    height:50,
    width:325,
    marginTop:15,
    marginLeft:10,
    padding:5,
  },myText:{
     color:'#00f',
    borderColor:'#007aff',
    fontSize:20,
    paddingTop:10,
    paddingBottom:10,
    fontWeight:'600',
  },

  buttonStyle: {
    width:100,
    height:50,
    borderWidth:1,
    alignItems:'center',
    borderRadius:5,
    backgroundColor:'#fff',
    borderColor:'#007aff',
    shadowOpacity:0.8,
    marginRight:15,
    marginTop:25,
  },
  myButton:{
    flexDirection:'column',
    alignItems:'flex-end',
    marginLeft:240,
  }

};
export default ImageDisplay;
