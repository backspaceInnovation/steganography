import React, { Component } from 'react';
import Header from './Header';
import ImageDisplay from'./ImageDisplay';
import {StackNavigator}  from 'react-navigation';
import SecondScreen from './SecondScreen';

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity,
} from 'react-native';

class MainScreen extends Component{

	static navigationOptions={
		title:'Home',
	};
	render(){
		return(
			<View>
		        
		        <ImageDisplay />
	      	</View>
		);
	}
}


export default MainScreen;