import React,{Component} from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	Image,
	Alert,
	AsyncStorage,
	TextInput
} from 'react-native';
import fetch_blob from 'react-native-fetch-blob';
import {StackNavigator} from 'react-navigation';
import RNFS from 'react-native-fs';
class DecodeScreen extends Component{
	constructor(props){
		super(props);
	}
	state={
		message:'',
		key:'',
		base64Image:'',
		imageLocation:'',
	}
	static navigationOptions={
		title:'Decode',
	};
	setMessage(text){
		this.setState({
			message:text,
		});
	}
	setKey(text){
		this.setState({
			key:text,
		});
	}
	componentDidMount(){
		this.assignValues();
	}
	async assignValues(){
		const value=await AsyncStorage.getItem('@MyBase64Image');
	    const imageSource=await AsyncStorage.getItem('@MyImageName');
	    this.setState({
		    base64Image:value,
		    imageLocation:imageSource,
	    });
	}
	async exampleTest(){
		const imageSource=await AsyncStorage.getItem('@MyImageName');
		Alert.alert(imageSource);
	};

	async tempFunction(){
		var RNFS=require('react-native-fs');
		var imageSource=await AsyncStorage.getItem('@MyImageName');
		var imageBase64=await AsyncStorage.getItem('@MyBase64Image');
		var path=RNFS.DocumentDirectoryPath+ '/hahahah.png';
		RNFS.writeFile(path,imageBase64,'utf-8')
		.then((success) => {
    		Alert.alert('FILE WRITTEN!');
  		})
  		.catch((err) => {
    		Alert.alert(err.message);
  		});

	}

	async showAlert(){
		/*var base64ToImage=require('base64-to-image');
		var imageSource=await AsyncStorage.getItem('@MyImageName');
		var imageBase64=await AsyncStorage.getItem('@MyBase64Image');
		var fs = fetch_blob.fs;
      	var dirs = fetch_blob.fs.dirs;
      	var file_path = dirs.DCIMDir + '/haha3.png';
      	var optionalObj = {'fileName': 'imageFileName', 'type':'png'};
      	var imageInfo=this.base64ToImage(imageBase64,file_path,optionalObject).bind(this);
      	Alert.alert('Info-'+imageInfo);*/

		/*var imageSource=await AsyncStorage.getItem('@MyImageName');
		var textLength=imageSource.length;
		var requiredString=imageSource.charAt(3);
		var temp=imageSource.indexOf('data');
		var tempString=imageSource.substr(1,3);
		var base64ImageLength=this.state.base64Image.length;
		var nextImage=this.state.base64Image.substr(1,80000);
		var restImage=this.state.base64Image.substr(80000,this.state.base64Image.length);

		//Alert.alert(''+base64ImageLength);
		this.setState({
			base64Image:nextImage+''+restImage,
		});
		try{
			const fs = fetch_blob.fs;
      		const dirs = fetch_blob.fs.dirs ;
      		const file_path = dirs.DCIMDir + "/haha3.jpeg";
			var img=this.state.base64Image;
			var data = img.replace(/^data:image\/\w+;base64,/, "");
			RNFS.writeFile(file_path, img, 'base64')
      		.catch((error) => {
        		alert(JSON.stringify(error));
      		});

			
		}catch(error){
			Alert.alert(''+error);
		}*/
	}
	render(){
		const {navigate}=this.props.navigation;
		var stringImage='data:image/jpeg;base64,'+ this.state.base64Image;
 
    	return(
	      	<View style={styles.myView}>
	        
		        <View style={styles.ImageContainer}>
	        	<Image style={styles.ImageContainer}
		        	 source={{uri:stringImage}} />

		        </View>
		        <TextInput style={styles.messages} 
		        underlineColorAndroid='transparent' 
		        placeholder='Enter the secrect key'
		        onChangeText={(text) =>this.setKey(text)} value={(this.state.text)}/>
		        <View style={styles.myButton}>
		            <TouchableOpacity style={styles.buttonStyle} 
		              onPress={this.tempFunction.bind(this)}>
		             <Text style={styles.myText}>
		                Next
		              </Text>
		            </TouchableOpacity>
		        </View>
	   		</View>
		);
  	}
	
}
const styles={
	myView:{
    backgroundColor:'#56a0d3',
    justifyContent:'center',
    alignItems:'center',
    flex:1,
    paddingTop:5,
    elevation:2,
    position:'relative',
  },
   ImageContainer: {
      borderRadius: 10,
      marginTop:20,
      width: 250,
      height: 280,
      borderColor: '#9B9B9B',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      
    },
    messages:{
      flexDirection:'row',
      alignItems:'center',
      textAlign:'center',
      padding:5,
      width: 250,
      backgroundColor:'#fff',
      marginTop:10,
      borderWidth:2,
      borderColor:'#00f',
      borderRadius:10,
    },

    buttonStyle: {
    width:100,
    height:50,
    borderWidth:1,
    alignItems:'center',
    borderRadius:5,
    backgroundColor:'#fff',
    borderColor:'#007aff',
    shadowOpacity:0.8,
    marginRight:15,
    marginTop:45,
  },
  myButton:{
    flexDirection:'column',
    alignItems:'flex-end',
    marginLeft:240,
  },myText:{
     color:'#00f',
    borderColor:'#007aff',
    fontSize:20,
    paddingTop:10,
    paddingBottom:10,
    fontWeight:'600',
  },

}
export default DecodeScreen;