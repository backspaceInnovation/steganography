import React from 'react';
import {Text,View} from 'react-native'
const Header=(props)=>{
  return(
    <View style={styles.myView}>
      <Text style={styles.myText}>
        {props.getHeader}
      </Text>
    </View>
  );
}
const styles={
  myText:{
    fontSize:20,
    justifyContent:'center',
    alignItems:'center',
  },
  myView:{
    backgroundColor:'#56a0d3',
    justifyContent:'center',
    alignItems:'center',
    height:50,
    paddingTop:5,
    elevation:2,
    position:'relative',
  },
}
export default Header;
