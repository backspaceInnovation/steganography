import React,{Component} from 'react';
import {Text,View,TouchableOpacity,Alert} from 'react-native';
const MyButton=(props)=>{

  return(
    <TouchableOpacity style={styles.buttonStyle} 
    onPress={props.onPress}>
      <Text style={styles.textStyle}>
        {props.buttonText}
      </Text>
    </TouchableOpacity>
  );
}
const styles={
  buttonStyle:{
    width:100,
    height:50,
    borderWidth:1,
    alignItems:'center',
    borderRadius:5,
    backgroundColor:'#fff',
    borderColor:'#007aff',
    shadowOpacity:0.8,
    marginLeft:5,
    marginRight:5,
    marginTop:175,
    position:'relative'
  },
  textStyle:{
    color:'#00f',
    borderColor:'#007aff',
    fontSize:20,
    paddingTop:10,
    paddingBottom:10,
    fontWeight:'600'
  }
};
export default MyButton;
