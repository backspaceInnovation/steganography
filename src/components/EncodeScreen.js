import React, { Component } from 'react';
import {Text,
  View,
  Image,
  Alert,
  TextInput,
  TouchableOpacity,
  AsyncStorage} from 'react-native'
import { StackNavigator } from 'react-navigation';



class EncodeScreen extends Component{

  constructor(props){
    super(props);
  }
  state={
    message:'',
    key:'',
    base64Image:'',
    imageSource:'',
  };
  static navigationOptions={
    title:'Encode',
  };

  setMessage(text){
    this.setState(
      {message:text},
    );
  }
  setKey(text){
    this.setState({
      key:text,
    })
  }
  
  async showAlert(){
    try{
      const value=await AsyncStorage.getItem('@MyBase64Image');
      const imageLocation=await AsyncStorage.getItem('@MyImageName');
      this.setState({
        base64Image:value,
        imageSource:imageLocation,

      });
      
      console.log("base64Message",value);
    }catch(error){

    }
  }
  
  render(){
    const {navigate}=this.props.navigation;
 
    return(
      <View style={styles.myView}>
        
        <View style={styles.ImageContainer}>
          <Image style={styles.ImageContainer} source={this.props.navigation.state.params.img} />
        </View>
        <Text>{this.props.navigation.state.params.imageBaseSources}</Text>
        <TextInput style={styles.messages}
        underlineColorAndroid='transparent' 
        placeholder='Enter the secrect message'
        onChangeText={(text) =>this.setMessage(text)} value={(this.state.text)}/>
        <TextInput style={styles.messages} 
        underlineColorAndroid='transparent' 
        placeholder='Enter the secrect key'
        onChangeText={(text) =>this.setKey(text)} value={(this.state.text)}/>
        <View style={styles.myButton}>
            <TouchableOpacity style={styles.buttonStyle} 
              onPress={this.showAlert.bind(this)}>
             <Text style={styles.myText}>
                Next
              </Text>
            </TouchableOpacity>
        </View>
    </View>

    );
  }
}


const styles={
  myView:{
    backgroundColor:'#56a0d3',
    justifyContent:'center',
    alignItems:'center',
    flex:1,
    paddingTop:5,
    elevation:2,
    position:'relative',
  },
   ImageContainer: {
      borderRadius: 10,
      marginTop:20,
      width: 250,
      height: 280,
      borderColor: '#9B9B9B',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      
    },
    messages:{
      flexDirection:'row',
      alignItems:'center',
      textAlign:'center',
      padding:5,
      width: 250,
      backgroundColor:'#fff',
      marginTop:10,
      borderWidth:2,
      borderColor:'#00f',
      borderRadius:10,
    },

    buttonStyle: {
    width:100,
    height:50,
    borderWidth:1,
    alignItems:'center',
    borderRadius:5,
    backgroundColor:'#fff',
    borderColor:'#007aff',
    shadowOpacity:0.8,
    marginRight:15,
    marginTop:25,
  },
  myButton:{
    flexDirection:'column',
    alignItems:'flex-end',
    marginLeft:240,
  },myText:{
     color:'#00f',
    borderColor:'#007aff',
    fontSize:20,
    paddingTop:10,
    paddingBottom:10,
    fontWeight:'600',
  },
}
export default EncodeScreen;
