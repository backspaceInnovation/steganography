import React, {Component} from 'react';
import {Text,View,Alert} from 'react-native';
import MyButton from './MyButton';
import Stegano from './Stegano';
import ImagePicker from './ImagePicker';
import StackNavigator from 'react-navigation';

class FrontPage extends Component{
  static NavigationOption={
    title:'Welcome',
  };


  constructor(props){
    super(props);
  }
 OnPressNextButton=() => {
   Alert.alert('You tapped the next button');
 }
 OnPressBackButton=() => {
   Alert.alert('You tapped the back button');
 }
 onPressDecode=()=>{
  this.navigatior.push({
    id:'ImagePicker'
  })
 }
 onPressEncode=()=>{
    navigate('ImageChoose', { name: 'Image' })
 }

 render(){
  const { navigate } = this.props.navigation;
  return(
    <View style={styles.wholeBody}>
      <View style={styles.body}>
        <View style={styles.setMargin}>
          <Stegano getBodyText="Decode Image" onPress={this.onPressDecode}/> 
        </View>
        <View style={styles.encode}>
          <Stegano getBodyText="Encode Image" onPress={this.onPressEncode}/> 
        </View>
      </View>
      <View style={styles.viewStyle}>
        <View>
          <MyButton  buttonText="Back" 
          onPress={this.OnPressBackButton}/>
        </View>
        <View style={styles.marginStyle}>
          <MyButton  
          buttonText="Next" onPress={this.OnPressNextButton} />
        </View>
      </View>
    </View>
  );
}
}const styles={
  viewStyle:{
    flexDirection:'row',
    position:'relative'
  },marginStyle:{
    marginLeft:128
  },encode:{
    marginTop:1
  },body:{
    flexDirection:'column',
    position:'relative'
  },setMargin:{
    marginTop:140
  },wholeBody:{
    borderWidth:1,
    borderBottomWidth:0,
    borderRadius:2,
    borderColor:'#ddd',
    shadowColor:'#000',
    shadowOffset:{width:0, height:2},
    shadowOpacity:0.2,
    marginLeft:5,
    marginRight:5,
    marginTop:5,
    elevation:1,
    position:'relative'
  }
};

export default FrontPage;
